# Maintainer: Yassine Oudjana (Tooniis) <y.oudjana@protonmail.com>
# Kernel config based on: arch/arm64/configs/defconfig

_flavor="postmarketos-qcom-msm8996"
pkgname=linux-$_flavor
pkgver=5.13.1
pkgrel=0
pkgdesc="Kernel close to mainline with extra patches for Qualcomm MSM8996 devices"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/msm8996-mainline/linux-msm8996"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-nftables"
makedepends="bison findutils flex installkernel openssl-dev perl"

# Source
_tag=v${pkgver//_/-}-msm8996
source="
	linux-msm8996-$_tag.tar.gz::$url/-/archive/$_tag/linux-msm8996-$_tag.tar.gz
	config-$_flavor.$arch
"
builddir="$srcdir/linux-msm8996-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz-$_flavor"

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
}
sha512sums="
c58a33e943012f3256cb75c1e98cbd448b99f2994c1a8bb5ed20d41933ea60d2859402e14a836bcaa1b63e64f0cc45f2f0c83008dedcf53f73529aebf1162465  linux-msm8996-v5.13.1-msm8996.tar.gz
bf844e53559e3d8180bb4bb44c49f92ffbafcb6e6535485ae009dcca1903b872f623e4ff58432536b220a834c3a7076e90969c0660d7e55f5c2ec4ee874be733  config-postmarketos-qcom-msm8996.aarch64
"
